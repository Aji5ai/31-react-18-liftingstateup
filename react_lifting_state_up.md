# React - Lifting State Up

Vous allez utiliser vos connaissance acquises des props et du state pour faire passer des informations entre deux composants

## Ressources

- [https://gitlab.com/bastienapp/react18-liftingstateup](https://gitlab.com/bastienapp/react18-liftingstateup)
​- [Lifting State Up & Prop Drilling in React](https://medium.com/@kristinethejohnson/lifting-state-up-prop-drilling-in-react-3ef3367fca7a)
- [What Is "Lifting State Up" in React?](https://www.freecodecamp.org/news/what-is-lifting-state-up-in-react/)

## Contexte du projet

Une collègue a démarré une application, et n'a pas eu le temps de la finir. Il s'agit d'une simulation de connexion utilisateur.

Elle te donne le lien de son dépôt et te demande de le finir en son absence. Afin de t'aider, elle a commenté les endroits où tu devrais intervenir.

Commence par faire un fork du dépôt suivant : [https://gitlab.com/bastienapp/react18-liftingstateup](https://gitlab.com/bastienapp/react18-liftingstateup).

- le site possède un composant `Navigation`, qui devra afficher le texte "Please log in" si l'utilisateur n'est pas connecté, et qui doit afficher le nom de l'utilisateur si ce dernier est connecté
- un composant `Profile` simule la connexion/déconnexion de l'utilisateur : pas besoin de le coder, il est déjà terminé.
- une fois que l'utilisateur se connecte, un objet contenant ses informations est remonté au composant parent `App` par "state lifting".​

Ta mission est donc de faire descendre les informations de l'utilisateur au composant `Navigation` par "props drilling", et d'afficher le nom de l'utilisateur s'il est connecté, ou le texte "Please log in" sinon.​

Ajoute des commentaires afin d'expliquer ton code.

Tu trouveras des informations sur la notion de "lifting State Up" et le "props drilling" dans les ressources suivantes :

​- [Lifting State Up & Prop Drilling in React](https://medium.com/@kristinethejohnson/lifting-state-up-prop-drilling-in-react-3ef3367fca7a)
- [What Is "Lifting State Up" in React?](https://www.freecodecamp.org/news/what-is-lifting-state-up-in-react/)


## Modalités pédagogiques

- Un dépôt GitLab contient le code du projet
- Faire un fork du dépôt et cloner le fork en local
- Étudier le composant "Profile" (sans le modifier) pour voir comment les informations de l'utilisateur sont passées au composant parent "App" par "state lifting"
- Modifier le composant App afin de passer les états de l'utilisateur au composant Navigation par "props drilling"
- Modifier le composant Navigation pour utiliser l'état de l'utilisateur pour afficher son nom quand il est connecté, ou pour afficher "Please login" dans le cas contraire

## Modalités d'évaluation

- Le texte dans le header change en fonction de la connexion de l'utilisateur
- Des commentaires expliquent le code
- Aucune erreur n'apparaît dans le terminal

## Livrables

- Un lien vers GitLab

## Critères de performance

- Le code source est documenté
- Utiliser les normes de codage du langage
- La documentation technique de l’environnement de travail est comprise
- Utiliser un outil de gestion de versions