import {useState} from "react";
import "./App.css";
import Navigation from "./components/Navigation";
import Profile from "./components/Profile";

function App() {
  const [user, setUser] = useState(null);

  return (
    <div className="App">
      <Navigation user={user} />{" "}
      {/* Navigation n'a besoin que de user, pas de le modifier. Lorsque Profile utilise setUser pour mettre à jour l'utilisateur, React réagit en réexécutant le composant App avec la nouvelle valeur de user.
      Cela entraîne également la réexécution de tous les composants enfants, y compris Navigation. Navigation reçoit alors la nouvelle valeur de user en tant que prop.*/}
      <Profile user={user} setUser={setUser} />
    </div>
  );
}

export default App;
