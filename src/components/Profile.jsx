import PropTypes from "prop-types";

function Profile(props) {
  const { user, setUser } = props;

  function login() {
    setUser({
      name: "Apolline", /* On donne un nom ici */
    });
  }

  function logout() { /* au clic sur le btn Logout appelle cette fonction qui remet à false user */
    setUser(null);
  }

  return (
    <div className="Profile">
      <h1>My profile</h1>
      {user ? ( /* Si user est true alors on fait ce qui suit */
        <div>
          <h3>Welcome {user?.name}!</h3>{" "}
          {/* signifie : "accéder à la propriété name de l'objet user, mais seulement si user n'est pas nul ou indéfini".  */}
          <button type="button" onClick={logout}>
            Logout
          </button>
        </div>
      ) : ( /* sinon si user false on fait la page de connexion */
        <div>
          <input type="text" value="Apolline" disabled /> {/* ne sert à rien de mettre user?.name ici car on est dans le cas ou user est false (donc ne contient pas de texte) */}
          <br />
          <input type="password" value="*************" disabled />
          <br />
          <button type="button" onClick={login}>
            Login
          </button>
        </div>
      )}
    </div>
  );
}

Profile.propTypes = {
  user: PropTypes.object,
  setUser: PropTypes.func.isRequired,
};

export default Profile;
