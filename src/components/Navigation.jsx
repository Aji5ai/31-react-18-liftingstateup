import "./Navigation.css";

function Navigation({user}) { /* On décompose directement ici car une seule propriété à passer, + court que faire une autre ligne const {user} = props */
  // TODO get user from props drilling
  return (
    <ul className="Navigation">
      <li>
        <img src="https://via.placeholder.com/200x100" alt="Logo" />
      </li>
      <li>
        <h2>Awesome website!</h2>
      </li>
      {/* TODO check if user is connected */}
      <li>
        {user ? `You are well connected ${user.name} :)` : "Please log in"}
      </li>
    </ul>
  );
}

export default Navigation;
